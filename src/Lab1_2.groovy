alphabetUpper = ('A'..'Z').toList()
alphabetLower = ('a'..'z').toList()

String encrypt(String phrase, int key) {
    phrase.collect { c -> encryptOne(c, key) }.join()
}

String decrypt(String phrase, int key) {
    phrase.collect { c -> decryptOne(c, key) }.join()
}

def encryptOne(c, key) {
    char chr = c as char
    def alphabet = chr.isLowerCase() ? alphabetLower
                                     : (chr.isUpperCase() ? alphabetUpper : [""])
    def ind = alphabet.indexOf(c)
    ind > -1 ? alphabet[(alphabet.indexOf(c) + key) % alphabet.size] : c
}

def decryptOne(c, key) {
    char chr = c as char
    def alphabet = chr.isLowerCase() ? alphabetLower
                                     : (chr.isUpperCase() ? alphabetUpper : [""])
    def ind = alphabet.indexOf(c)
    ind > -1 ? alphabet[ind - key] : c
}

//Test
(1..26).each { key ->
    (alphabetUpper + alphabetLower).each { c ->
        assert decryptOne(encryptOne(c, key), key) == c
    }
}

def bruteForce(String phrase) {
    println 'Possible answers:'
    (1..alphabetUpper.size).each { key ->
        println "Key: $key -> ${decrypt(phrase, key)}"
    }
}

println encrypt('This is the text!', 10)
println decrypt("Drsc sc dro dohd!", 10)
println()
bruteForce 'Drsc sc dro dohd!'

