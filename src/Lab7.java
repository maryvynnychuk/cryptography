import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class Lab7 {

    static SecureRandom secureRandom;

    public static void main(String[] args) throws NoSuchAlgorithmException {
        secureRandom = SecureRandom.getInstance("SHA1PRNG");

        BigInteger n = BigInteger.probablePrime(1024, secureRandom);
        BigInteger g = generateOneToNMinusOne(n);
        BigInteger a = generateOneToNMinusOne(n);

        System.out.println("n = " + n);
        System.out.println("g = " + g);
        System.out.println("a = " + a);

        BigInteger h = g.modPow(a, n);
        System.out.println("h = " + h);

        String text = "Quick Brown Fox Jumped Over A Lazy Dog";
        System.out.println("Text: " + text);

        BigInteger[] encrypted = elGamalEncrypt(n, g, h, text);

        System.out.println("encrypted: " + Arrays.toString(encrypted));

        String decrypted = elGamalDecrypt(encrypted, a, n);

        System.out.println("decrypted: " + decrypted);
    }

    static BigInteger generateOneToNMinusOne(BigInteger n) {
        return new BigInteger(160, secureRandom)
                .mod(n.subtract(BigInteger.valueOf(2)))
                .add(BigInteger.ONE);
    }

    static BigInteger[] elGamalEncrypt(BigInteger n, BigInteger g, BigInteger h, String text) {
        BigInteger m = new BigInteger(text.getBytes());

        BigInteger r = generateOneToNMinusOne(n);
        BigInteger c1 = g.modPow(r, n);
        BigInteger c2 = h.modPow(r, n).multiply(m).mod(n);

        return new BigInteger[] {c1, c2};
    }

    static String elGamalDecrypt(BigInteger[] message, BigInteger a, BigInteger n) {
        BigInteger c1 = message[0];
        BigInteger c2 = message[1];
        BigInteger m = c1.modPow(a, n).modInverse(n).multiply(c2).mod(n);

        return new String(m.toByteArray());
    }
}
