import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab2 {

    private static final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) {

        String text = "Quick Brown Fox Jumped Over A Lazy Dog";
        String key = "MODIqnCNSjcnsjnaaq";

        String encrypted = vigenereEncrypt(key, text);

        System.out.println(String.format("Encrypted '%s' -> '%s'", text, encrypted));

        String decrypted = vigenereDecrypt(key, encrypted);

        System.out.println(String.format("Decrypted '%s' -> '%s'", encrypted, decrypted));
    }

    private static String vigenereEncrypt(String key, String text) {
        validate(key);
        validate(text);

        String longKey = multipliedKey(key, text.length());
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            int Pi = ALPHABET.indexOf(text.charAt(i));
            int Ki = ALPHABET.indexOf(longKey.charAt(i));
            int c = (Pi + Ki) % ALPHABET.length();
            result.append(ALPHABET.charAt(c));
        }

        return result.toString();
    }

    private static String vigenereDecrypt(String key, String encrypted) {
        validate(key);
        validate(encrypted);

        String longKey = multipliedKey(key, encrypted.length());
        StringBuilder result = new StringBuilder();
        final int L = ALPHABET.length();

        for (int i = 0; i < encrypted.length(); i++) {
            int Pi = ALPHABET.indexOf(encrypted.charAt(i));
            int Ki = ALPHABET.indexOf(longKey.charAt(i));
            int c = (Pi + L - Ki) % L;
            result.append(ALPHABET.charAt(c));
        }

        return result.toString();
    }

    private static void validate(String s) {
        if (s.chars().anyMatch(c -> ALPHABET.indexOf(c) < 0)) {
            throw new RuntimeException(s + " contains characters outside the alphabet");
        }
    }

    private static String multipliedKey(String key, int len) {
        return Stream.generate(key::chars)
                .flatMapToInt(Function.identity())
                .limit(len)
                .mapToObj(c -> String.valueOf((char) c))
                .collect(Collectors.joining());
    }
}
