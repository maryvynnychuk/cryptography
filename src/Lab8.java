import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

public class Lab8 {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");

        BigInteger p = BigInteger.probablePrime(128, secureRandom);
        BigInteger q = BigInteger.probablePrime(128, secureRandom);

        System.out.println("P = " + p);
        System.out.println("Q = " + q);

        BigInteger n = p.multiply(q);
        BigInteger phiN = p.subtract(ONE).multiply(q.subtract(ONE));

        System.out.println("N = " + n);
        System.out.println("phiN = " + phiN);

        BigInteger e = getPublicKey(secureRandom, n, phiN, p, q);

        BigInteger privateKey = getPrivateKey(e, n, phiN);

        System.out.println("e = " + e);
        System.out.println("privateKey = " + privateKey);

        String text = "The text";
        BigInteger m = new BigInteger(text.getBytes());

        BigInteger signature = sign(m, privateKey, n);

        System.out.println(signature);
        System.out.println(checkSignature(m, signature, e, n));
    }

    static BigInteger getPublicKey(SecureRandom random, BigInteger n, BigInteger phiN, BigInteger p, BigInteger q) {
        BigInteger e;
        do {
            e = new BigInteger(n.bitCount(), random).mod(n.subtract(ONE)).add(ONE);
        } while (!e.gcd(phiN).equals(ONE));

        return e;
    }

    static BigInteger getPrivateKey(BigInteger e, BigInteger n, BigInteger phiN) {
        BigInteger inv = e.modInverse(phiN);
        if (inv.compareTo(ZERO) < 0) {
            inv = phiN.add(inv);
        }

        return inv;
    }

    static BigInteger sign(BigInteger m, BigInteger d, BigInteger n) {
        return m.modPow(d, n);
    }

    private static boolean checkSignature(BigInteger m, BigInteger signature, BigInteger e, BigInteger n) {
        BigInteger mt = signature.modPow(e, n);
        return mt.equals(m);
    }
}
