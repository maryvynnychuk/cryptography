import java.util.Arrays;

public class Lab4 {

    public static void main(String[] args) {

        System.out.println(inverseElement(3, 17));
        System.out.println(inverseElement2(3, 17));
        System.out.println(Arrays.toString(gcdex(3, 14)));
        System.out.println(phi(4));
    }

    static long gcd(long a, long b) {
        while (b != 0) {
            long t = b;
            b = a % b;
            a = t;
        }
        return a;
    }

    static long[] gcdex(long a, long b) {
        if (b == 0) {
            return new long[] {a, 1, 0};
        }

        long[] dxy = gcdex(b, a % b);

        return new long[] { dxy[0], dxy[2], dxy[1] - a / b * dxy[2] };
    }

    static boolean isPrime(long a) {
        for (int i = 2; i < Math.sqrt(a); i++) {
            if (a % i == 0) {
                return false;
            }
        }
        return true;
    }

    static long inverseElement(long a, long m) {
        return gcdex(a, m)[1];
    }

    static long inverseElement2(long a, long m) {
        long k = 1;
        while ((m * k + 1) % a != 0) {
            ++k;
        }
        return (m * k + 1) / a;
    }

    static long phi(long m) {
        long mm = m - 1;

        long count = 0;

        while (m - count != 0) {
            if (gcd(mm, m) == 1) {
                ++count;
            }
            --mm;
        }
        return count;
    }
}
