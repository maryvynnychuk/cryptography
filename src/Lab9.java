import java.util.Scanner;

public class Lab9 {

    private static final long mod = 0b100011011;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Write hex number to multiply");
        long num = scanner.nextLong(16);

        System.out.println("*02");
        System.out.println(Long.toString(mul02(num), 16).toUpperCase());

        System.out.println("*03");
        System.out.println(Long.toString(mul03(num), 16).toUpperCase());

        System.out.println("Print 2 numbers to multiply");
        long a = scanner.nextLong(16);;
        long b = scanner.nextLong(16);;

        System.out.println(Long.toString(gmul(a, b), 16).toUpperCase());
    }

    static long mul02(long n) {
        return (n << 1) ^ mod;
    }

    static long mul03(long n) {
        return mul02(n) ^ n;
    }

    static long gmul(long a, long b) {
        long p = 0;
        for (int i = 0; i < 8; i++) {
            if ((b & 1) != 0) {
                p ^= a;
            }
            long hiBits = a & 0x80;
            a <<= 1;
            a &= 0xFF;
            if (hiBits != 0) {
                a ^= 0x1b;
            }
            b >>= 1;
        }
        return p;
    }
}
