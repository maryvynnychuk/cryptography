import java.util.Random;

import static java.lang.String.format;

public class Lab5 {
    static final String ALPHABET =
            " ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "abcdefghijklmnopqrstuvwxyz";
    static final int M = ALPHABET.length();
    static final Random random = new Random();

    public static void main(String[] args) {
        final int a = chooseA();
        final int b = random.nextInt(M);

        System.out.println("m = " + M);
        System.out.println("a = " + a);
        System.out.println("b = " + b);

        String text = "Quick Brown Fox Jumped Over A Lazy Dog";
        String encrypted = affineEncrypt(text, a, b);

        System.out.println(format("'%s' -> '%s'", text, encrypted));
        String decrypted = affineDecrypt(encrypted, a, b);
        System.out.println(format("'%s' -> '%s'", encrypted, decrypted));

    }

    static String affineEncrypt(String text, int a, int b) {
        StringBuilder result = new StringBuilder();
        for (char c : text.toCharArray()) {
            result.append(encryptOne(c, a, b));
        }
        return result.toString();
    }

    static String affineDecrypt(String text, int a, int b) {
        StringBuilder result = new StringBuilder();
        for (char c : text.toCharArray()) {
            result.append(decryptOne(c, a, b));
        }
        return result.toString();
    }

    static char encryptOne(char c, int a, int b) {
        int x = ALPHABET.indexOf(c);
        if (x == -1) {
            throw new RuntimeException("Character '" + c
                    + "' doesn't belong to the alphabet " + ALPHABET);
        }
        int index = (a * x + b) % M;
        return ALPHABET.charAt(index);
    }

    static char decryptOne(char c, int a, int b) {
        int x = ALPHABET.indexOf(c);
        if (x == -1) {
            throw new RuntimeException("Character '" + c
                    + "' doesn't belong to the alphabet " + ALPHABET);
        }
        int a_1 = (int) Lab4.inverseElement2(a, M);
        int index = (a_1 * (x - b)) % M;
        if (index < 0) index = M + index;
        return ALPHABET.charAt(index);
    }

    static int chooseA() {
        int result = M;

        while (Lab4.gcd(result, M) > 1) {
            result = random.nextInt(M);
        }
        return result;
    }
}
